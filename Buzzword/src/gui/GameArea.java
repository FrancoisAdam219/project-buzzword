package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import data.BuzzBoard;
import data.GameData;
import data.SessionData;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.EventTarget;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.beans.EventHandler;
import java.util.TimerTask;

/**
 * Created by Francois on 11/26/2016.
 */
public class GameArea {
	/*
	 * This class keeps the current session variables together. This is where the game will be played.
	 */
	public AppTemplate 			app;					//reference to Workspace.app
	public GameInfoArea			gameInfo;				//reference to Workspace.gameInfoArea

	private VBox				playBox;				//play area for game
	private VBox				infoBox;				//info area for game
	private WordGrid			wordGrid;				//grid of letters for game
	private Label				lbMode;					//title of mode
	private Label				lbLevel;				//title of level
	private Label				lbTimer;				//timer display; shows time left in session
	private SessionData			theSession;				//board from controller

	private Button				btPlay;					//play/pause level
	private Button				btReset;				//reset level
	private Button				btQuit;					//go back to level select (has confirm)
	private Button				btNext;					//go to next level

	public GameArea (AppTemplate initApp, GameInfoArea infoArea){
		this.app = initApp;
		this.gameInfo = infoArea;
		lbMode = new Label("Test Mode");
		lbMode.getStyleClass().add("mode-label");
		wordGrid = new WordGrid();
		lbLevel = new Label("Level 1");
		lbLevel.getStyleClass().add("mode-label");
		btPlay = new Button("PLAY"); //Fixme: play icon image
		btReset = new Button("RESET"); //Fixme: reset icon image
		btQuit = new Button("QUIT");
		btNext = new Button("NEXT");
		lbTimer = new Label("Time Remaining: 10s");
	}

	public void setupBubbleHandlers(){
		Bubble[][] bubbles = wordGrid.bubble;

		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++){
				int tx = x; int ty = y;
				Bubble bub = bubbles[tx][ty];
				bub.circle.setOnDragDetected(e->{
					bub.circle.startFullDrag();
					dragOn(tx,ty,bub);
				});
				bub.circle.setOnMouseDragEntered(e->{dragOn(tx,ty,bub);});
				bub.circle.setOnMouseReleased(e->{dragOff(tx,ty,bub);});
			}
	}

	private void dragOn(int x, int y, Bubble bub){
		//System.out.println("Drag On: "+x+","+y);
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		ws.getController().gameHandler.handleGridClick(x,y,bub);
	}

	private void dragOff(int x, int y, Bubble bub){
		resetBubbles();
	}

	private void resetBubbles(){
		Bubble[][] bubbles = wordGrid.bubble;
		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++) {
				bubbles[x][y].circle.setEffect(null);
				bubbles[x][y].circle.setFill(Color.valueOf("rgb(200,200,200)"));
			}
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		ws.getController().checkWord();
	}

	public void applyToBorderPane(BorderPane pane){
		playBox = new VBox();
		playBox.setSpacing(20);
		playBox.setAlignment(Pos.TOP_CENTER);

		HBox pauseBox = new HBox(btQuit,btPlay,btNext);
		pauseBox.setSpacing(20);
		pauseBox.setAlignment(Pos.CENTER);

		playBox.getChildren().addAll(lbMode,wordGrid.pane,lbLevel,pauseBox);
		pane.setCenter(playBox);

		infoBox = new VBox();
		gameInfo.applyToVBox(infoBox);
		pane.setRight(infoBox);

		btPlay.setOnAction(e->{
			Workspace ws = (Workspace)app.getWorkspaceComponent();
			ws.getController().gameHandler.handlePlay();
		});
		btQuit.setOnAction(e->{
			Workspace ws = (Workspace)app.getWorkspaceComponent();
			ws.getController().gameHandler.handleQuit();
		});
		btNext.setOnAction(e->{
			Workspace ws = (Workspace)app.getWorkspaceComponent();
			ws.getController().gameHandler.handleNext();
		});
	}

	public void setContent(BuzzwordController.GameState state){
		switch(state){
			case INACTIVE_DEFAULT:{
				showSession(false);
				wordGrid.fixButtons();
				wordGrid.setBasic();
				break;
			}
			case ACTIVE_MODESEL:{
				showSession(false);
				wordGrid.hideAll();
				wordGrid.fixButtons();
				break;
			}
			case ACTIVE_LEVELSEL:{
				showSession(false);
				Workspace ws = (Workspace)app.getWorkspaceComponent();
				String lvName = ws.getController().getMode();
				lbMode.setText(lvName);
				int lvCount = ws.getController().getLevelCount();
				int lvCurr = ws.getController().getGameData().getModeInfo(lvName).level;
				wordGrid.levelDisplay(lvCurr,lvCount);
				break;
			}
			case ACTIVE_PAUSED:{
				setupBubbleHandlers();
				showSession(true);
				Workspace ws = (Workspace)app.getWorkspaceComponent();
				String lvName = ws.getController().getMode();
				lbMode.setText(lvName);
				int lvCount = ws.getController().getLevel();
				lbLevel.setText("Level: "+Integer.toString(lvCount));
				this.theSession = ws.getController().getSessionData();
				wordGrid.boardDisplay(theSession.getMyBoard().getBoard());
				wordGrid.showLetters(false);
				btPlay.setText("PLAY");
				gameInfo.updateInfo();
				gameInfo.setGuessText("");
				break;
			}
			case ACTIVE_PLAYING:{
				showSession(true);
				Workspace ws = (Workspace)app.getWorkspaceComponent();
				String lvName = ws.getController().getMode();
				lbMode.setText(lvName);
				int lvCount = ws.getController().getLevel();
				lbLevel.setText("Level: "+Integer.toString(lvCount));
				this.theSession = ws.getController().getSessionData();
				wordGrid.boardDisplay(theSession.getMyBoard().getBoard());
				wordGrid.showLetters(true);
				btPlay.setText("PAUSE");
				gameInfo.updateInfo();
				break;
			}
			case ACTIVE_ENDED:{
				btPlay.setText("RESET");
				break;
			}
		}
	}

	public WordGrid getWordGrid() {return wordGrid;}

	public GameInfoArea getGameInfo() {return gameInfo;}

	public void setTimeLeft(SimpleStringProperty t) {lbTimer.textProperty().bind(t);}

	private void showSession(boolean s){
		lbMode.setText("");
		lbLevel.setText("");
		btPlay.setVisible(s);
		btQuit.setVisible(s);
		btNext.setVisible(s);
		btReset.setVisible(s);
		gameInfo.showInfo(s);
	}

	public AnswerList getAnswerList() {return gameInfo.getAnswerList();}
}
