package gui;

import apptemplate.AppTemplate;
import data.SessionData;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Created by Francois on 12/4/2016.
 */
public class AnswerList {
	public AppTemplate app;					//reference to Workspace.app

	public static class Answer{
		public SimpleStringProperty word;
		public SimpleIntegerProperty score;

		public Answer(String tWord, int tScore){
			word = new SimpleStringProperty(tWord);
			score = new SimpleIntegerProperty(tScore);
		}

		public void setWord(String tWord) {word.set(tWord);}
		public String getWord() { return word.get();}

		public void setScore(int tScore){score.set(tScore);}
		public Integer getScore() { return score.get();}
	}

	private SessionData				watchSession;			//reference to session
	private TableView<Answer> 		table;					//table of words and their score
	private ObservableList<Answer> 	data;					//the data itself
	private TableColumn 			wordList,scoreList;		//columns for the table

	public AnswerList(AppTemplate initApp){
		this.app = initApp;
		table = new TableView<>();
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.setPrefWidth(160);
		data = FXCollections.observableArrayList();
		wordList = new TableColumn("TOTAL");
		wordList.setPrefWidth(90);
		wordList.setResizable(false);
		wordList.setSortable(false);
		wordList.setCellValueFactory(new PropertyValueFactory<Answer,String>("word"));
		scoreList = new TableColumn("100");
		scoreList.setPrefWidth(50);
		scoreList.setResizable(false);
		scoreList.setSortable(false);
		scoreList.setCellValueFactory(new PropertyValueFactory<Answer,Integer>("score"));
		table.setItems(data);
		table.getColumns().addAll(wordList,scoreList);
	}
	public void showList(boolean s){
		table.setVisible(s);
	}

	public void addAnswer(String word, int score){
		data.add(new Answer(word,score));
	}

	public void linkSession(SessionData sd) {watchSession = sd;}

	public void update(){
		data.clear();
		scoreList.setText(Integer.toString(watchSession.getTotalScore()));
		for(SessionData.AnswerPair p: watchSession.getAnswers()){
			addAnswer(p.word,p.score);
		}
		Workspace ws = (Workspace) app.getWorkspaceComponent();
		ws.getGameArea().getGameInfo().updateInfo();
	}

	public TableView<Answer> getAnswerTable(){ return table;}
}
