package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Created by Francois on 11/26/2016.
 */
public class MenuList {
	public AppTemplate          app;                    //Reference to Workspace.app

	public Label				lbProfName;				//current username
	private Button              btNewProfile;           //create new profile
	private Button              btLogin;                //load profile
	private Button       	    btLogout;               //close profile
	private Button				btEditProfile;			//edit opened profile
	private Button              btStartGame;            //start game session
	private Button				btHome;					//go back to home menu

	private ComboBox			cbGameMode;				//game mode combo box
	//Note: Usage Tip: cbGameMode.getItems().(...)

    public MenuList (AppTemplate initApp){
        app = initApp;
		lbProfName = new Label("NULL: HIDE ME!");
		lbProfName.setAlignment(Pos.CENTER);
		lbProfName.setPrefSize(150,30);
		btNewProfile = new Button ("Create Profile"); //Fixme: enum: MENU_CREATE_PROFILE
		btNewProfile.setPrefSize(150,30);
		btLogin = new Button ("Log In"); //Fixme: enum: MENU_LOGIN
		btLogin.setPrefSize(150,30);
		btLogout = new Button ("Log Out"); //Fixme: Game.currentUser
		btLogout.setPrefSize(150,30);
		btEditProfile = new Button ("Edit Profile");
		btEditProfile.setPrefSize(150,30);
		btStartGame = new Button ("Start Playing"); //Fixme: enum: MENU_START_GAME
		btStartGame.setPrefSize(150,30);
		btHome = new Button ("Home"); //Fixme: enum: MENU_HOME
		btHome.setPrefSize(150,30);
		cbGameMode = new ComboBox(); //Todo: add combo box contents
		cbGameMode.setPrefSize(150,30);
		cbGameMode.getItems().addAll("Option A","Option B","Option C");
    }

    public void applyToVBox(VBox menu){
		menu.setSpacing(20);
		menu.setPadding(new Insets(0,20,50,20));
		menu.getChildren().addAll(lbProfName,btNewProfile,btLogin,btLogout,btEditProfile,btStartGame,btHome,cbGameMode);
	}

	public void setupHandlers(BuzzwordController controller){
		btNewProfile.setOnAction(e->{
			controller.menuHandler.handleCreateProfile();
		});
		btLogin.setOnAction(e->{
			try {
				controller.menuHandler.handleLogin();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		btLogout.setOnAction(e->{
			controller.menuHandler.handleLogout();
		});
		btEditProfile.setOnAction(e->{
			controller.menuHandler.handleEditProfile();
		});
		btStartGame.setOnAction(e->{
			controller.menuHandler.handleStartGame();
		});
		btHome.setOnAction(e->{
			controller.menuHandler.handleHome();
		});
		cbGameMode.setOnAction(e->{
			if(cbGameMode.getValue()!=null)
				controller.menuHandler.handleGameModeSelect(cbGameMode.getValue().toString());
		});
	}

	public void setContent(VBox menu, BuzzwordController.MenuState state){
		menu.getChildren().clear();
		switch(state){
			case UNINITIALIZED:{
				menu.getChildren().addAll(btNewProfile,btLogin);
				break;
			}
			case INITIALIZED:{
				Workspace workspace = (Workspace)app.getWorkspaceComponent();
				lbProfName.setText(workspace.getController().getGameData().getUsername());
				menu.getChildren().addAll(lbProfName,btLogout,btEditProfile,btStartGame);
				break;
			}
			case PLAYING:{
				menu.getChildren().addAll(lbProfName,btHome,cbGameMode);
				break;
			}
		}
	}

	public void updateModeList(String[] args){
		cbGameMode.getItems().clear();
		for(String s: args){
			cbGameMode.getItems().add(s);
		}
	}

}
