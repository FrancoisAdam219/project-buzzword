package gui;

import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * Created by Francois on 11/30/2016.
 */
public class Bubble {
	public DropShadow glow = new DropShadow(15, Color.YELLOW);		//glow for objects
	public Circle circle;
	public Text letter;

	public Bubble(double px, double py, double r, String l){
		circle = new Circle(px,py,r);
		circle.setFill(Color.valueOf("rgb(200,200,200)"));
		circle.setOnMousePressed(e->{circle.setFill(Color.valueOf("rgb(140,140,140)"));});
		circle.setOnMouseReleased(e->{circle.setFill(Color.valueOf("rgb(200,200,200)"));});
		circle.setOnMouseEntered(e->{circle.setEffect(glow);});
		circle.setOnMouseDragExited(e->{circle.setEffect(null);});
		circle.setOnMouseExited(e->{circle.setEffect(null);});
		letter = new Text(px-5,py+5,l);
		letter.setMouseTransparent(true);
		letter.setTextAlignment(TextAlignment.RIGHT);
		letter.setFill(Color.BLACK);
	}
}
