package gui;

import controller.BuzzwordController;
import data.SessionData;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

/**
 * Created by Francois on 11/30/2016.
 */
public class WordGrid {

	public Group pane;				//main pane to place all objects
	public Bubble[][]			bubble;					//bubble containing text
	public Line[][]				lineH, lineV;			//line placed inbetween bubbles

	public WordGrid(){
		pane = new Group();
		Rectangle r = new Rectangle(-45,-45,300,300);
		r.setFill(Color.valueOf("rgb(160,94,47)"));
		pane.getChildren().add(r);
		// Create Lines
		lineH = new Line[3][4];
		for(int x=0;x<3;x++)
			for(int y=0;y<4;y++){
				int px = 25+(70*x);
				int py = (70*y);
				lineH[x][y] = new Line(px,py,px+20,py);
				lineH[x][y].setStroke(Color.BLACK);
				lineH[x][y].setFill(Color.BLACK);
				pane.getChildren().add(lineH[x][y]);
			}
		lineV = new Line[4][3];
		for(int x=0;x<4;x++)
			for(int y=0;y<3;y++){
				int px = (70*x);
				int py = 25+(70*y);
				lineV[x][y] = new Line(px,py,px,py+20);
				lineV[x][y].setStroke(Color.BLACK);
				lineV[x][y].setFill(Color.BLACK);
				pane.getChildren().add(lineV[x][y]);
			}
		// Create Bubbles
		bubble = new Bubble[4][4];
		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++){
				int px = (x*50) + (x*20);
				int py = (y*50) + (y*20);
				bubble[x][y] = new Bubble(px,py,25,x+","+y);
				pane.getChildren().addAll(bubble[x][y].circle,bubble[x][y].letter);
			}

	}

	public void setupHandlers(BuzzwordController cntr){
		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++){
				final int tx = x;
				final int ty = y;
				bubble[x][y].circle.setOnMouseClicked(e->{
					cntr.gameHandler.handleGridClick(tx,ty,bubble[tx][ty]);
				});
				bubble[x][y].letter.setOnMouseClicked(e->{
					cntr.gameHandler.handleGridClick(tx,ty,bubble[tx][ty]);
				});
			}
	}

	public void setBasic(){
		gridBuzzWord();
		showGrid(true);
		showLines(true);
	}

	public void hideAll(){
		showGrid(false);
		showLines(false);
	}

	public void levelDisplay(int cur, int cnt){
		showGrid(false);
		showLines(false);
		for(int a=0;a<cnt;a++){
			int x = a%4; int y = a/4;
			showCircle(x,y,Integer.toString(a+1));
			if(cur>=a){
				bubble[x][y].circle.setFill(Color.valueOf("rgb(200,200,200)"));
				bubble[x][y].circle.setMouseTransparent(false);
			}
			else {
				bubble[x][y].circle.setFill(Color.valueOf("rgb(140,140,140)"));
				bubble[x][y].circle.setMouseTransparent(true);
			}
		}
	}

	public Bubble getBubble(int x, int y){
		return bubble[x][y];
	}

	public void fixButtons(){
		for(Bubble[] bx: bubble) {
			for (Bubble by : bx) {
				by.circle.setFill(Color.valueOf("rgb(200,200,200)"));
				by.circle.setMouseTransparent(false);
			}
		}
	}

	public void highlightBubble(boolean[][] on){
		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++){
				if(on[x][y]){
					bubble[x][y].circle.setFill(Color.valueOf("rgb(140,140,140)"));
					bubble[x][y].circle.setEffect(new DropShadow(15, Color.YELLOW));
				}
			}
	}

	public void highlightBubble(boolean on){
		for(int x=0;x<4;x++)
			for(int y=0;y<4;y++){
				if(on){
					bubble[x][y].circle.setFill(Color.valueOf("rgb(140,140,140)"));
					bubble[x][y].circle.setEffect(new DropShadow(15, Color.YELLOW));
				} else {
					bubble[x][y].circle.setFill(Color.valueOf("rgb(200,200,200)"));
					bubble[x][y].circle.setEffect(null);
				}
			}
	}

	public void boardDisplay(char[][] board){
		for(int a=0;a<16;a++){
			int x = a%4; int y = a/4;
			showCircle(x,y,Character.toString(board[x][y]).toUpperCase());
		}
		showGrid(true);
		showLines(true);
	}

	public void showLetters(boolean s){
		for(Bubble[] bx: bubble){
			for(Bubble by: bx){
				by.letter.setVisible(s);
			}
		}
	}

	private void showGrid(boolean s){
		for(Bubble[] bx: bubble){
			for(Bubble by: bx){
				by.circle.setVisible(s);
				by.letter.setVisible(s);
			}
		}
	}

	private void showLines(boolean s){
		for(Line[] lhx: lineH){
			for(Line lhy: lhx){
				lhy.setVisible(s);
			}
		}
		for(Line[] lvx: lineV){
			for(Line lvy: lvx){
				lvy.setVisible(s);
			}
		}
	}

	private void gridBuzzWord(){
		setLetter(0,0,"B");		setLetter(1,0,"U");		setLetter(2,0,"Z");		setLetter(3,0," ");
		setLetter(0,1," ");		setLetter(1,1," ");		setLetter(2,1,"Z");		setLetter(3,1," ");
		setLetter(0,2," ");		setLetter(1,2,"W");		setLetter(2,2," ");		setLetter(3,2," ");
		setLetter(0,3," ");		setLetter(1,3,"O");		setLetter(2,3,"R");		setLetter(3,3,"D");
	}

	private void showCircle(int x, int y, String s){
		bubble[x][y].circle.setVisible(true);
		bubble[x][y].letter.setVisible(true);
		setLetter(x,y,s);
	}

	private void setLetter(int x, int y, String s){ bubble[x][y].letter.setText(s);}
}
