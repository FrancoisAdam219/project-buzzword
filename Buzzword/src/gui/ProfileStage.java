package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Francois on 12/4/2016.
 */
public class ProfileStage {
	public AppTemplate app;					//reference to Workspace.app

	public static class ProfInfo{
		private String user,pass;

		public ProfInfo(String u,String p){
			user = u; pass = p;
		}

		public String getUser() {return user;}
		public String getPass() {return pass;}
	}

	public Stage				stage;					//stage for the login window
	public Scene				scene;					//the scene

	private GridPane			pane;					//grid
	private Label				lbUsername;				// "Username"
	private Label				lbPassword;				// "Password"
	private TextField			tfUsername;				//username text input
	private PasswordField 		tfPassword;				//password text input
	private Button				btSubmit;				// submit button (all "Enter" maps to this button)

	public ProfileStage(AppTemplate initApp){
		this.app = initApp;
		pane = new GridPane();
		pane.setPadding(new Insets(50,30,50,30));
		pane.setHgap(20);pane.setVgap(15);
		lbUsername = new Label("Username: ");
		lbUsername.setPrefSize(120,20);
		lbPassword = new Label("Password: ");
		lbPassword.setPrefSize(120,20);
		tfUsername = new TextField();
		tfUsername.setPrefSize(160,20);
		tfPassword = new PasswordField();
		tfPassword.setPrefSize(160,20);
		btSubmit = new Button("Submit");
		btSubmit.setPrefSize(100,20);
	}

	public void layoutGUI(){
		pane.add(lbUsername,0,0);
		pane.add(lbPassword,0,1);
		pane.add(tfUsername,1,0);
		pane.add(tfPassword,1,1);
		pane.add(btSubmit,1,2);

		scene = new Scene(pane);
		scene.getStylesheets().add("css/buzzword_style.css");
		stage = new Stage();
		stage.setTitle("Profile Submission");
		stage.setScene(scene);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(app.getGUI().getWindow());
	}

	public ProfInfo getProfileInfo(){
		final ProfInfo[] profile = {new ProfInfo("", "")};
		btSubmit.setOnAction(e->{
			String user = tfUsername.getText();
			String pass = tfPassword.getText();
			if(user.length()>=4&&pass.length()>=4){
				profile[0] = new ProfInfo(user,pass);
				tfUsername.clear();
				tfPassword.clear();
				stage.close();
			}
		});
		tfUsername.setOnAction(e->{
			btSubmit.fire();
		});
		tfPassword.setOnAction(e->{
			btSubmit.fire();
		});
		stage.setOnCloseRequest(e->{
			profile[0] = null;
			tfUsername.clear();
			tfPassword.clear();
			stage.close();
		});
		stage.showAndWait();
		return profile[0];
	}
}
