package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import data.DictionaryData;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Modality;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppGUI;

import javax.swing.*;
import javax.swing.event.MenuListener;
import java.util.HashMap;
import java.util.Optional;

/**
 * Created by Francois on 11/17/2016.
 */
public class Workspace extends AppWorkspaceComponent{

    AppTemplate                 app;                    //The base application
    AppGUI                      gui;                    //Raw GUI from app

    GameArea                    gameArea;               //This is the area for holding the gameplay portion
	GameInfoArea				gameInfoArea;			//This is the area that keeps track of progress in a session
    MenuList                    menuList;               //Container for the menu bar on the left side (controls buttons)
    BuzzwordController          controller;             //Main controller of the app
	ProfileStage				profStage;				//Stage for profile login/logout/edit

	private Stage 				mainStage;
	private Scene				mainScene;
	private BorderPane			mainPane;				//outside border pane
	private VBox				menuBox;				//mainPane.left
	private BorderPane			gameBox;				//mainPane.center

	private Label 				lbHeader;				//"!! Buzz Word !!"


    public Workspace(AppTemplate initApp) {
        app = initApp;
        gui = app.getGUI();
        controller = new BuzzwordController(app);
        layoutGUI();            // create all of the GUI components
        setupHandlers();        // then immediately initialize the event handling
		sortContent();			// after everything is prepared, sort the GUI to display home information
    }

    private void layoutGUI() {
		profStage = new ProfileStage(app);
		gui.getAppPane().setTop(null);
		profStage.layoutGUI();

		//Todo: Idea: create a progress window to show all progress
		//PropertyManager propertyManager = PropertyManager.getManager();
		//Todo: implement properties via XML.
		lbHeader = new Label("!! Buzz Word !!"); //Fixme: enum: APP_HEADER
		lbHeader.getStyleClass().add("header-label");

		menuList = new MenuList(app);
		gameInfoArea = new GameInfoArea(app);
		gameArea = new GameArea(app,gameInfoArea);

		menuBox = new VBox();
		menuList.applyToVBox(menuBox); //Todo: set buttons according to BuzzwordController.menuState

		gameBox = new BorderPane();
		gameArea.applyToBorderPane(gameBox);

		mainPane = new BorderPane();
		mainPane.setTop(lbHeader);
		mainPane.setAlignment(lbHeader, Pos.TOP_CENTER);
		mainPane.setLeft(menuBox);
		mainPane.setCenter(gameBox);
		gui.getAppPane().setTop(lbHeader);
		gui.getAppPane().setCenter(mainPane);
		//Todo: stage.setOnCloseRequest(e ->{})
	}

    private void setupHandlers() {
		menuList.setupHandlers(this.controller);
		gameArea.getWordGrid().setupHandlers(controller);
		Platform.setImplicitExit(false);
		gui.getWindow().setOnCloseRequest(e->{
			if(controller.menuState.equals(BuzzwordController.MenuState.PLAYING)){
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
				alert.setTitle("Confirmation Dialog");
				alert.setHeaderText("Closing Game");
				alert.setContentText("Are you sure you want to quit this session?");
				alert.initModality(Modality.WINDOW_MODAL);
				alert.initOwner(gui.getWindow());
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK){
					controller.handleCloseApp();
					gui.getWindow().close();
					Platform.exit();
				}
				else{
					e.consume();
				}
			}
			else{
				controller.handleCloseApp();
				gui.getWindow().close();
				Platform.exit();
			}
		});
		gui.getWindow().getScene().setOnKeyPressed(e->{
			String codeString = e.getCode().toString();
			//System.out.println(codeString);
			char c = codeString.toLowerCase().charAt(0);
			if(!e.getCode().equals(KeyCode.ENTER)&&
					Character.isLetter(c)&&controller.gameState.equals(BuzzwordController.GameState.ACTIVE_PLAYING)){
				controller.getSessionData().handleKeyLetter(c);
			}
			if(e.getCode().equals(KeyCode.ENTER)&&controller.gameState.equals(BuzzwordController.GameState.ACTIVE_PLAYING)){
				controller.getSessionData().checkAnswer();
			}
		});
	}

    private void sortContent() {
		setContent(controller.menuState,controller.gameState);
	}

	public void setContent(BuzzwordController.MenuState menuState, BuzzwordController.GameState gameState){
		if(menuState.equals(BuzzwordController.MenuState.PLAYING)&&gameState.equals(BuzzwordController.GameState.ACTIVE_MODESEL))
			menuList.updateModeList(controller.getDictionaryNames());

		menuList.setContent(menuBox,menuState);

		gameArea.setContent(gameState);
	}

	public ProfileStage.ProfInfo getProfileInfo(){
		ProfileStage.ProfInfo profile = profStage.getProfileInfo();
		return profile;
	}

	public BuzzwordController getController(){ return controller;}

	public GameArea getGameArea() {return gameArea;}

	public AnswerList getAnswerList() {return gameArea.getAnswerList();}

    @Override
    public void initStyle() {
		//PropertyManager propertyManager = PropertyManager.getManager();
    }

    @Override
    public void reloadWorkspace() {

    }
}
