package gui;

import apptemplate.AppTemplate;
import data.SessionData;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.toIntExact;

/**
 * Created by Francois on 11/27/2016.
 */
public class GameInfoArea {
	/*
	 * This class keeps track of the guessed words and scores for the current game session.
	 */

	public AppTemplate 			app;					//reference to Workspace.app

	private AnswerList			answerList;				//list of guessed answers
	private Label				lbTimer;				//timer of session
	private Label				lbGuess;				//current guess in progress
	private Label				lbTarg;					// "Target"
	private Label				lbLetters;			//target score of level

	public GameInfoArea(AppTemplate initApp){
		this.app = initApp;
		lbTimer = new Label("Time Remaining: 10s"); //Todo: Game.sessionTimer
		lbTimer.setPrefWidth(160);
		lbTimer.getStyleClass().add("time-label");
		lbGuess = new Label("G U E S S"); //Todo: Game.currentGuess
		lbGuess.setPrefWidth(160);
		lbGuess.getStyleClass().add("word-label");
		answerList = new AnswerList(app);
		lbTarg = new Label("Target: 150 Points"); //Fixme: enum: GINFO_LABEL_TARGET
		lbTarg.setPrefWidth(160);
		lbTarg.getStyleClass().add("word-label");
		lbLetters = new Label("Minimum Letters: 3"); //Todo: Game.sessionGoal + "Points"
		lbLetters.setPrefWidth(160);
		lbLetters.getStyleClass().add("word-label");
	}

	public void applyToVBox(VBox box){
		box.setSpacing(20);
		box.setPadding(new Insets(0,30,50,20));
		box.getChildren().addAll(lbTimer,lbGuess,answerList.getAnswerTable(),lbTarg,lbLetters);
	}

	public void showInfo(boolean s){
		lbTimer.setVisible(s);
		lbGuess.setVisible(s);
		answerList.showList(s);
		lbTarg.setVisible(s);
		lbLetters.setVisible(s);
	}

	public void updateInfo(){
		Workspace ws = (Workspace) app.getWorkspaceComponent();
		SessionData sd = ws.getController().getSessionData();
		int timeSec = toIntExact(sd.getTimer());
		lbTimer.setText("Time Remaining: "+timeSec+"s");
		lbGuess.setText(sd.getGuessSpaced());
		lbTarg.setText("Target Score: "+Integer.toString(ws.getController().getScoreReq()));
		lbLetters.setText("Minumum Letters: "+Integer.toString(ws.getController().getMinLetters()));
	}

	public void setGuessText(String s){
		lbGuess.setText(s);
	}

	public AnswerList getAnswerList() {return answerList;}
}
