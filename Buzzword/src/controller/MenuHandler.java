package controller;

import gui.ProfileStage;
import gui.Workspace;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Optional;

/**
 * Created by Francois on 11/26/2016.
 */
public class MenuHandler {
    /*
     * This class mostly serves as a separation from the main controller class BuzzwordController.
     * Most of the menu handling can be seen here, instead of making BuzzwordController so directly large.
     */

    public BuzzwordController 			controller;				//head controller (BuzzwordController)
	BuzzwordController.MenuState 		menuState;				//head menu state
	BuzzwordController.GameState 		gameState;				//head game state

    public MenuHandler (BuzzwordController controller){
		this.controller = controller;
	}

	private void checkState(){
		menuState = controller.menuState;
		gameState = controller.gameState;
	}

	public void handleCreateProfile (){
		checkState();
		controller.createGameData(handleProfInfo());
	}

	public void handleLogin() throws Exception {
		checkState();
		controller.loadGameData(handleProfInfo());
	}

	public void handleLogout(){
		checkState();
		controller.closeGameData();
	}

	public void handleEditProfile(){
		checkState();
		controller.editGameData(handleProfInfo());
	}

	public void handleStartGame(){
		checkState();
		controller.setMenuState(BuzzwordController.MenuState.PLAYING);
		controller.setGameState(BuzzwordController.GameState.ACTIVE_MODESEL);
		controller.updateWorkspace();
	}

	public void handleHome(){
		checkState();
		if(gameState!= BuzzwordController.GameState.INACTIVE_DEFAULT){
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Go Back to Home");
			alert.setContentText("Are you sure about that?");
			alert.initModality(Modality.WINDOW_MODAL);
			alert.initOwner(controller.appTemplate.getGUI().getWindow());
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK){
				controller.setMenuState(BuzzwordController.MenuState.INITIALIZED);
				controller.setGameState(BuzzwordController.GameState.INACTIVE_DEFAULT);
				controller.updateWorkspace();
			}
		}
		else{
			controller.setMenuState(BuzzwordController.MenuState.INITIALIZED);
			controller.setGameState(BuzzwordController.GameState.INACTIVE_DEFAULT);
			controller.updateWorkspace();
		}
	}

	public void handleGameModeSelect(String mode){
		checkState();
		if(gameState!= BuzzwordController.GameState.INACTIVE_DEFAULT){
			controller.selectMode(mode);
			controller.setGameState(BuzzwordController.GameState.ACTIVE_LEVELSEL);
			controller.updateWorkspace();
		}
	}

	private ProfileStage.ProfInfo handleProfInfo(){
		Workspace workspace = (Workspace) controller.appTemplate.getWorkspaceComponent();
		ProfileStage.ProfInfo gameProfile = workspace.getProfileInfo();
		return gameProfile;
	}

}
