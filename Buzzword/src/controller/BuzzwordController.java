package controller;

import apptemplate.AppTemplate;
import com.sun.xml.internal.stream.Entity;
import com.sun.xml.internal.ws.api.pipe.FiberContextSwitchInterceptor;
import data.*;
import gui.AnswerList;
import gui.Bubble;
import gui.ProfileStage;
import gui.Workspace;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.stage.Modality;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.*;
import java.util.concurrent.RunnableFuture;
import java.util.stream.Stream;

/**
 * Created by Francois on 11/26/2016.
 */
public class BuzzwordController implements FileController{

    public enum GameState {
        INACTIVE_DEFAULT,        //No game active, display default letters
        ACTIVE_MODESEL,          //Game active, mode has not been selected (hide grid)
        ACTIVE_LEVELSEL,         //Game active, mode is selected (display grid as level choice)
        ACTIVE_PLAYING,          //Game active, session started (generate letter grid and show info)
        ACTIVE_PAUSED,			//Game active, but paused (hide grid and guess, but show everything else)
		ACTIVE_ENDED			//game is active, but it is over. EndState takes over
    }

    public enum MenuState {
        UNINITIALIZED,           //Nothing has loaded yet...
        INITIALIZED,             //Loaded profile, can now select game mode
        PLAYING                  //Playing game, selecting mode (gamestate takes over here)
    }

    public enum EndState {
        WIN,
		LOSE
    }

    public AppTemplate         appTemplate;            //Game application reference
    private GameData            gameData;               //Game data manager reference
	private String				currentDic;				//Currently selected dictionary. used for GUI and data checks.
	private int					currentLevel;			//Currently selected level.
	private Set<DictionaryData> dictionaries;			//Dictionaries in the "words" folder
	private SessionData			sessionData;			//Data of current session (discardable)
    private Path                savePath;               //Path of the current save file
    public MenuHandler          menuHandler;            //Menu mechanic handler
    public GameHandler          gameHandler;            //Game mechanic handler
	private Timer 				gameTimer = new Timer();
	private TimerTask 			timerTask;
	private int					intTimer;
	private SimpleStringProperty	sendTimer;

    public MenuState            menuState;              //State of the overall menu
    public GameState            gameState;              //State of the current game in progress

    public BuzzwordController(AppTemplate initApp) {
        this.appTemplate = initApp;
        this.menuState = MenuState.UNINITIALIZED;
        this.gameState = GameState.INACTIVE_DEFAULT;
        this.menuHandler = new MenuHandler (this);
        this.gameHandler = new GameHandler (this);
		dictionaries = new HashSet<>();
		loadDictionaries();
		timerTask = new TimerTask() {
			@Override
			public void run() {
				Platform.runLater(() -> {
					if(gameState.equals(GameState.ACTIVE_PLAYING)){
						sessionData.timer--;
						if(sessionData.timer<=-1)
							endSession();
						updateWorkspace();
					}
				});
			}
		};
		gameTimer.scheduleAtFixedRate(timerTask,0,1000);
    }

    public void createGameData(ProfileStage.ProfInfo profile){
		if(profile != null){
			gameData = new GameData(appTemplate);
			gameData.createNew(profile);
			GameDataFile gdf = new GameDataFile();
			try {
				gdf.saveData(gameData,null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			menuState = MenuState.INITIALIZED;
			updateWorkspace();
		}
    }

    public void loadGameData(ProfileStage.ProfInfo profile) throws Exception {
		if(profile!=null){
			gameData = new GameData(appTemplate);
			GameDataFile gdf = new GameDataFile();
			if(gdf.checkInfo(profile)){
				gameData.createNew(profile);
				gdf.loadData(gameData,null);
				menuState = MenuState.INITIALIZED;
			}
			else{
				Alert alert = new Alert(Alert.AlertType.WARNING);
				alert.setTitle("Warning Dialog");
				alert.setHeaderText("Incorrect Details");
				alert.setContentText("Either the username or password is incorrect.");
				alert.initOwner(appTemplate.getGUI().getWindow());
				alert.initModality(Modality.WINDOW_MODAL);
				alert.showAndWait();
			}
			updateWorkspace();
		}
    }

    public void closeGameData(){
        gameData.reset();
        menuState = MenuState.UNINITIALIZED;
        updateWorkspace();
    }

    public void editGameData(ProfileStage.ProfInfo profile){
		if(profile!=null){
			gameData.editInfo(profile);
			GameDataFile gdf = new GameDataFile();
			try {
				gdf.saveData(gameData,null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			menuState = MenuState.INITIALIZED;
			updateWorkspace();
		}

    }

    public void updateWorkspace(){
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        workspace.setContent(menuState,gameState);
    }

    public void loadDictionaries(){
		try(Stream<Path> paths = Files.walk(Paths.get(new File("").getAbsolutePath()+"/Buzzword/resources/words"))) {
			paths.forEach(filePath -> {
				if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".json")) {
					//System.out.println(filePath);
					DictionaryData d = new DictionaryData(appTemplate,filePath.toString());
					d.loadWords();
					boolean dupeCheck = false;
					for(DictionaryData dt: dictionaries){
						if(dt.getName().matches(d.getName())){
							dupeCheck = true; break;
						}
					}
					if(!dupeCheck)
						dictionaries.add(d);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void handlePlayButton(){
		switch(gameState){
			case ACTIVE_PAUSED:{
				setGameState(GameState.ACTIVE_PLAYING);
				updateWorkspace();
				break;
			}
			case ACTIVE_PLAYING:{
				setGameState(GameState.ACTIVE_PAUSED);
				updateWorkspace();
				break;
			}
			case ACTIVE_ENDED:{
				createSession();
				break;
			}

		}
	}

	public void handleResetButton(){

	}

	public void handleCloseApp(){
		System.out.println("Closing...");
		gameTimer.cancel();
	}

	public void applyLetter(int x, int y, Bubble b){
		sessionData.applyMouseLetter(x,y,b);
		updateWorkspace();
	}

	public void checkWord(){
		sessionData.checkAnswer();
		updateWorkspace();
	}

	public String[] getDictionaryNames(){
		String[] result = new String[dictionaries.size()];
		int iter = 0;
		for(DictionaryData d: dictionaries){
			result[iter++] = d.getName();
		}
		return result;
	}

	public void selectMode(String mode){
		currentDic = mode;
		DictionaryData dic = null;
		for(DictionaryData d: dictionaries){
			if(d.getName().matches(mode)){
				dic = d; break;
			}
		}
		if(dic != null)
			gameData.prepModeInfo(dic);
	}

	public boolean nextLevelAvailable(int l){
		return gameData.getModeInfo(currentDic).level >= l&&gameData.getModeInfo(currentDic).level<= getLevelCount();
	}

	public boolean selectLevel(int l){
		boolean lGood = false;

		if(nextLevelAvailable(l)){
			//System.out.println("checked!");
			currentLevel = l+1;lGood = true;
		}
		return lGood;
	}

	public void createSession(){
		sessionData = new SessionData(appTemplate,getWordList());
		Workspace ws = (Workspace)appTemplate.getWorkspaceComponent();
		ws.getGameArea().getWordGrid().fixButtons();
		AnswerList al = ws.getAnswerList();
		sessionData.linkAnswerList(al);
		gameState = GameState.ACTIVE_PAUSED;
		intTimer = 30;
		updateWorkspace();
	}

	public void endSession(){
		gameState = GameState.ACTIVE_ENDED;
		boolean isScoreMet = sessionData.giveScoreCheck();
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("End Game Status");
		alert.setHeaderText(null);
		alert.initModality(Modality.WINDOW_MODAL);
		alert.initOwner(appTemplate.getGUI().getWindow());
		if(isScoreMet){ //Game WIN
			alert.setContentText("Congratulations, you won!\nYour Score is: "+sessionData.getTotalScore());
			if(gameData.getModeInfo(currentDic).level<=getLevel()) {
				gameData.getModeInfo(currentDic).level++;
				GameDataFile gdf = new GameDataFile();
				try {
					gdf.saveData(gameData,null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		else { //Game LOSE
			alert.setContentText("Oh, so close!\nYour Score is: "+sessionData.getTotalScore());
		}
		alert.showAndWait();
		updateWorkspace();
	}

	public void handleQuitSession(){
		menuHandler.handleGameModeSelect(currentDic);
	}

	public int getLevelCount() {
		int result = 0;
		for(DictionaryData d: dictionaries)
			if(d.getName().matches(currentDic))
				result = d.getLevelCount();
		return result;
	}

	public int getLevel() { return currentLevel;}
    public String getMode(){ return currentDic;}

	public int getScoreReq() {
		int result = 0;
		for(DictionaryData d: dictionaries)
			if(d.getName().matches(currentDic)){
				//System.out.println("Check: "+currentLevel);// + ": "+d.getLevel(currentLevel).toString());
				result = d.getLevel(currentLevel).targetScore;
			}
		return result;
	}

	public Set<String> getWordList(){
		Set<String> result= new HashSet<>();
		for(DictionaryData d: dictionaries)
			if(d.getName().matches(currentDic))
				result = d.getWords();
		return result;
	}

	public int getMinLetters() {
		int result = 0;
		for(DictionaryData d: dictionaries)
			if(d.getName().matches(currentDic))
				result = d.getLevel(currentLevel).letterMin;
		return result;
	}

	public GameData getGameData() {return gameData;}
	public SessionData getSessionData() {return sessionData;}

	public void setMenuState(MenuState m) {menuState = m;}
	public void setGameState(GameState g) {gameState = g;}

    @Override
    public void handleNewRequest() {
        //Unused
    }

    @Override
    public void handleSaveRequest() throws IOException {
        //Unused
    }

    @Override
    public void handleLoadRequest() throws IOException {
        //Unused
    }

    @Override
    public void handleExitRequest() {
        //Todo: Use for btQuit
    }
}
