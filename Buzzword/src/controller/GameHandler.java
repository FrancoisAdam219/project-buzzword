package controller;

import gui.Bubble;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;

import java.util.Optional;

/**
 * Created by Francois on 11/26/2016.
 */
public class GameHandler {
    /*
     * This class serves as a separation from BuzzwordController.
     * This class holds most of the game handling methods, such as keyboard input, mouse input, etc.. Game stuff.
     */
	public BuzzwordController			controller;				// head controller (BuzzwordController)
	BuzzwordController.MenuState 		menuState;				//head menu state
	BuzzwordController.GameState 		gameState;				//head game state

	public GameHandler (BuzzwordController controller){
		this.controller = controller;
	}

	public void handleGridClick(int x, int y, Bubble b){
		checkState();
		switch(gameState){
			case ACTIVE_LEVELSEL:{
				if(controller.selectLevel(Integer.parseInt(b.letter.getText())-1)){
					controller.createSession();
				}
				break;
			}
			case ACTIVE_PLAYING:{
				controller.applyLetter(x,y,b);
				break;
			}
		}
	}

	private void checkState(){
		menuState = controller.menuState;
		gameState = controller.gameState;
	}

	public void handlePlay(){
		checkState();
		controller.handlePlayButton();
	}

	public void handleQuit(){
		checkState();
		switch(gameState){
			case ACTIVE_PAUSED:
			case ACTIVE_PLAYING:{
				controller.setGameState(BuzzwordController.GameState.ACTIVE_PAUSED);
				controller.updateWorkspace();
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
				alert.setTitle("Confirmation Dialog");
				alert.setHeaderText("Quitting the Session");
				alert.setContentText("Are you sure about that?");
				alert.initModality(Modality.WINDOW_MODAL);
				alert.initOwner(controller.appTemplate.getGUI().getWindow());

				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK){
					controller.handleQuitSession();
				}
				break;
			}
			case ACTIVE_ENDED:{
				controller.handleQuitSession();
				break;
			}
		}
	}

	public void handleNext(){
		checkState();
		switch(gameState){
			case ACTIVE_PAUSED:
			case ACTIVE_PLAYING:{
				if(controller.nextLevelAvailable(controller.getLevel())){
					controller.setGameState(BuzzwordController.GameState.ACTIVE_PAUSED);
					controller.updateWorkspace();
					Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
					alert.setTitle("Confirmation Dialog");
					alert.setHeaderText("Skipping to Next Level");
					alert.setContentText("Are you sure about that?");
					alert.initModality(Modality.WINDOW_MODAL);
					alert.initOwner(controller.appTemplate.getGUI().getWindow());

					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == ButtonType.OK){
						if(controller.selectLevel(controller.getLevel()))
							controller.createSession();
					}
				}
				break;
			}
			case ACTIVE_ENDED:{
				if(controller.nextLevelAvailable(controller.getLevel())){
					if(controller.selectLevel(controller.getLevel()))
						controller.createSession();
				}
				break;
			}
		}
	}

	public void handleReset(){
		checkState();
		controller.handleResetButton();
	}


}
