package data;

import apptemplate.AppTemplate;
import com.sun.javafx.sg.prism.NGShape;
import components.AppDataComponent;
import gui.ProfileStage;

import java.security.MessageDigest;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Francois on 11/17/2016.
 */
public class GameData implements AppDataComponent{

    public AppTemplate app;             // Workspace.app

    public static class ModeProgress{
        public String name;
        public int level;
        public int[] topScore;

        public ModeProgress(String n, int l){
            name = n; level = l;
        }
        public ModeProgress(String n){
            name = n; level = 0;
        }
    }

    private String              username;               // Profile username
    private String              firstUser;              // last username
    private String              password;               // Profile password
    private String              firstPass;              // last password
    private Set<ModeProgress>   progress;          // Profile Progress in all known modes


    public GameData(AppTemplate initApp){
        app = initApp;
        progress = new HashSet<>();
    }

    public void createNew(ProfileStage.ProfInfo profile){
        username = profile.getUser();
        password = profile.getPass();
        firstUser = profile.getUser();
        firstPass = profile.getPass();
    }

    public void editInfo(ProfileStage.ProfInfo profile){
        firstUser = username;
        firstPass = password;
        username = profile.getUser();
        password = profile.getPass();
    }

    public void prepModeInfo(DictionaryData mode){
        boolean modeCheck = false;
        for(ModeProgress mp: progress){
            if(mp.name.matches(mode.getName())){
                modeCheck = true; break;
            }
        }
        if(!modeCheck) progress.add(new ModeProgress(mode.getName()));
    }

    public ModeProgress getModeInfo(String modeName) {
        ModeProgress result = new ModeProgress("Null");
        for(ModeProgress mp: progress){
            if(mp.name.matches(modeName)){
                result = mp; break;
            }
        }
        return result;
    }
    public String getUsername() {return username;}
    public String getFirstUser() {return firstUser;}
    public void setUsername(String s) {username = s;firstUser = s;}
    public String getPassword() {return password;}
    public String getFirstPass() {return firstPass;}
    public void setPassword(String s) {password = s;firstPass = s;}
    public Set<ModeProgress> getProgress() {return progress;}
    public void setProgress(Set<ModeProgress> s) {progress = s;}

    @Override
    public void reset() {
        username = "";
        password = "";
        firstUser = "";
        firstPass = "";
        progress = new HashSet<>();
    }
}
