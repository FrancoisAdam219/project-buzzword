package data;

import apptemplate.AppTemplate;
import gui.AnswerList;
import gui.Bubble;
import gui.Workspace;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import jdk.nashorn.internal.ir.LiteralNode;
import sun.awt.image.ImageWatched;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Francois on 12/9/2016.
 */
public class SessionData {

	public AppTemplate app;             // Workspace.app
	private AnswerList			watchAnswerList;
	private BuzzBoard			myBoard;

	public class AnswerPair{
		public String word;
		public int score = 0;

		public AnswerPair(String w){
			word = w;
			calcScore();
		}

		private void calcScore(){
			score = 10+((word.length()-3)*5);
		}
	}

	public static class GuessNode{
		public int x, y;
		Character c;

		public GuessNode(int tx, int ty, Character tc) {x = tx; y = ty; c = tc;}
	}

	public int					timer;					//game timer
	private Set<AnswerPair>		answers;				//proper answers in session with score
	private int					totalScore;				//score of the session
	private boolean				usingKeys = false;

	private boolean[][] 					chosen;				//grid of characters
	private ArrayList<GuessNode> 			guessList;
	private ArrayList<LinkedList<GuessNode>>	guessKeyList;
	private String							trueKeys;

	public SessionData(AppTemplate initApp, Set<String> words){
		long timeStart = System.currentTimeMillis();
		app = initApp;
		answers = new HashSet<>();
		myBoard = generateBoard(words);
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		int scoreReq = ws.getController().getScoreReq();
		while(getTopScore(myBoard.getTrueAnswers())<scoreReq)
			myBoard = generateBoard(words);
		long timeEnd = System.currentTimeMillis();
		//System.out.println("Time: "+(timeEnd-timeStart)/1000.f);
		totalScore = 0;
		timer = 30;
	}

	public void linkAnswerList(AnswerList al) {watchAnswerList = al; watchAnswerList.linkSession(this);update();}

	public void update(){
		totalScore = 0;
		for(AnswerPair a: answers){
			totalScore+= a.score;
			watchAnswerList.addAnswer(a.word,a.score);
		}
		watchAnswerList.update();
	}

	public void handleKeyLetter(char c){
		if(!usingKeys) {checkAnswer();usingKeys=true;}

		trueKeys+=c;
		System.out.println("Word: "+trueKeys);
		generatePaths(trueKeys,true);

		Workspace ws = (Workspace) app.getWorkspaceComponent();
		ws.getGameArea().getWordGrid().highlightBubble(false);
		ws.getGameArea().getWordGrid().highlightBubble(chosen);
	}

	private void generatePaths(String s,boolean init){
		if(s.length()>0){
			char c = s.charAt(0);
			String str = s.substring(1);
			if(init){
				guessKeyList = new ArrayList<>();
				System.out.println("New String with: "+c);
				boolean[][] buttonCheck = myBoard.checkKey(c);
				for(int x=0;x<4;x++)
					for(int y=0;y<4;y++){
						if(buttonCheck[x][y]){
							GuessNode tNode = new GuessNode(x,y,c);
							LinkedList<GuessNode> tNodeList = new LinkedList<>();
							tNodeList.addLast(tNode);
							guessKeyList.add(tNodeList);
						}
					}
			}
			else{
				System.out.println("Adding to String: "+c);
				Iterator<LinkedList<GuessNode>> it = guessKeyList.iterator();
				boolean stopper = false;
				while(it.hasNext()&&stopper == false){
					boolean remover = false;
					LinkedList<GuessNode> cNodeList = it.next();
					boolean[][] buttonCheck = myBoard.checkKey(c);
					for(int x=0;x<4;x++)
						for(int y=0;y<4;y++){
							if(buttonCheck[x][y]){
								if(!chosen[x][y]&&checkAdjacency(x,y,cNodeList)) {
									cNodeList.addLast(new GuessNode(x, y, c));
								}
								else{
									remover = true;
								}
							}
						}
					if(remover) it.remove();
					else stopper = true;
				}
			}
			for(int a=0;a<16;a++){
				int tx = a%4;int ty = a/4;
				chosen[tx][ty] = false;
			}
			for(LinkedList<GuessNode> listing: guessKeyList){
				// get all currently selected nodes and flag chosen for true by coordinate
				for(GuessNode g: listing){
					System.out.println("Lit: "+g.x+","+g.y);
					chosen[g.x][g.y]=true;
				}
			}
			generatePaths(str,false);
		}else if(guessKeyList.size()==0){
			trueKeys = "";
		}
	}

	public void applyMouseLetter(int x, int y, Bubble b){
		if(usingKeys) {checkAnswer();usingKeys=false;}
		applyLetter(x,y,b);
	}

	public void applyLetter(int x, int y, Bubble b){
		if(!chosen[x][y]&&checkAdjacency(x,y,guessList)){
			guessList.add(new GuessNode(x,y,myBoard.getBoard()[x][y]));
			chosen[x][y]=true;
			Workspace ws = (Workspace) app.getWorkspaceComponent();
			ws.getGameArea().getWordGrid().highlightBubble(chosen);
		}
	}
	public boolean checkAdjacency(int x, int y,ArrayList<GuessNode> list){
		if(list.size()>0){
			GuessNode g = list.get(list.size()-1);
			return (g.x+1==x || g.x == x || g.x-1==x) && (g.y+1==y || g.y == y || g.y-1==y);
		}
		else
			return true;
	}
	public boolean checkAdjacency(int x, int y,LinkedList<GuessNode> list){
		if(list.size()>0){
			GuessNode g = list.getLast();
			return (g.x+1==x || g.x == x || g.x-1==x) && (g.y+1==y || g.y == y || g.y-1==y);
		}
		else
			return true;
	}

	public String checkLink(ArrayList<GuessNode> list){
		String guessStr = "";
		for(GuessNode g: list){
			guessStr += Character.toString(g.c);
		}
		return guessStr;
	}

	public void checkAnswer(){
		String guessStr = "";
		for(GuessNode g: guessList){
			guessStr += Character.toString(g.c);
		}
		System.out.println("Guessing: "+guessStr);
		if(myBoard.getTrueAnswers().contains(guessStr)){
			boolean addIt = true;
			for(AnswerPair p: answers){
				if(p.word.matches(guessStr)){
					addIt = false; break;
				}
			}
			if(addIt){
				//System.out.println("Good!");
				answers.add(new AnswerPair(guessStr));
			}
		}
		resetChecks();
		update();
	}

	public void resetChecks(){
		//System.out.println("RESET!!");
		chosen = new boolean[4][4];
		guessList = new ArrayList<>();
		guessKeyList = new ArrayList<>();
		trueKeys = "";
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		ws.getGameArea().getWordGrid().highlightBubble(false);
	}

	public Set<AnswerPair> getAnswers() {return answers;}
	public String getGuessSpaced() {
		String guessStr = "";
		for(GuessNode g: guessList){
			guessStr += Character.toString(g.c).toUpperCase()+" ";
		}
		return guessStr;
	}

	public BuzzBoard generateBoard(Set<String> words){
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		BuzzBoard result = new BuzzBoard(4, words, ws.getController().getMinLetters());
		chosen = new boolean[4][4];
		guessList = new ArrayList<>();
		guessKeyList = new ArrayList<>();
		trueKeys = "";
		return result;
	}

	public static int getTopScore(Set<String> words){
		int result = 0;
		for(String s: words)
			result+= 10+((s.length()-3)*5);
		//System.out.println(result);
		return result;
	}


	public boolean giveScoreCheck() {
		Workspace ws = (Workspace)app.getWorkspaceComponent();
		int scoreReq = ws.getController().getScoreReq();
		return totalScore >= scoreReq;
	}
	public int getTimer() {return timer;}
	public int getTotalScore() {return totalScore;}
	public BuzzBoard getMyBoard() {return myBoard;}
}
