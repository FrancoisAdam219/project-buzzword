package data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Francois on 12/10/2016.
 */
public class BuzzBoard {

	private char[][]			board;				//board being generated
	private int					boardSize;			//size of the board, squared
	private Set<String>			trueAnswers;		//all true answers in relationship to a dictionary
	private Set<String>			dictionary;			//set of words from dictionary
	private int 				minLet = 3;				//minimum letter count

	public BuzzBoard(int size, Set<String> words, int minLetter){
		Random r = new Random();
		boardSize = size;
		dictionary = words;
		minLet = minLetter;
		board = new char[boardSize][boardSize];
		for(int x=0;x<boardSize;x++)
			for(int y=0;y<boardSize;y++){
				int c = r.nextInt(26) + (byte)'a';
				board[x][y] = (char)c;
			}
		solve();
	}

	public BuzzBoard(char[][] manualBoard, Set<String> words, int minLetter){
		board = manualBoard;
		minLet = minLetter;
		solve();
	}

	public void solve(){
		//System.out.println("Answer List");
		trueAnswers = new HashSet<>();
		boolean[][] checker = new boolean[boardSize][boardSize];
		for(int x=0;x<boardSize;x++)
			for(int y=0;y<boardSize;y++)
				checker[x][y] = false;
		for(int x=0;x<boardSize;x++)
			for(int y=0;y<boardSize;y++){
				checkChar(x,y,checker,"");
			}
	}

	private void checkChar(int x, int y, boolean[][] ch, String oldStr){
		String result = oldStr;
		if(inBounds(x,y))
		if(!ch[x][y]){
			result+=board[x][y];
			ch[x][y] = true;
			boolean[][] chCopy = new boolean[ch.length][];
			for(int i=0;i<ch.length;i++)
				chCopy[i] = Arrays.copyOf(ch[i],ch[i].length);
			checkChar(x-1,y-1,chCopy,result);
			checkChar(x,y-1,chCopy,result);
			checkChar(x+1,y-1,chCopy,result);
			checkChar(x-1,y,chCopy,result);
			checkChar(x+1,y,chCopy,result);
			checkChar(x-1,y+1,chCopy,result);
			checkChar(x,y+1,chCopy,result);
			checkChar(x+1,y+1,chCopy,result);
		}
		if(result.length()>=minLet&&dictionary.contains(result)&&!trueAnswers.contains(result)){
			//System.out.println(result); //Note: used for answer checking
			trueAnswers.add(result);
		}
	}

	private boolean inBounds(int x, int y){
		return x>=0 && y>=0 && x<boardSize && y<boardSize;
	}

	public boolean[][] checkKey(char c){
		boolean[][] result = new boolean[4][4];
		for(int x=0;x<4;x++) {
			for (int y = 0; y < 4; y++)
				result[x][y] = board[x][y] == c;
		}
		return result;
	}

	public String boardString(){
		String result = "-BOARD-\n";
		for(char[] cx: board){
			for(char ct: cx){
				result+="["+ct+"]";
			}
			result+="\n";
		}
		return result;
	}

	public char[][] getBoard() {return board;}
	public Set<String> getTrueAnswers() {return trueAnswers;}
}
