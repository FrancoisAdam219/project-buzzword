package data;

import components.AppDataComponent;
import components.AppFileComponent;
import gui.ProfileStage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.toIntExact;

/**
 * Created by Francois on 11/17/2016.
 */
public class GameDataFile implements AppFileComponent{

    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String PROGRESS = "PROGRESS";
    public static final String PROG_NAME = "PROG_NAME";
    public static final String PROG_LEVEL = "PROG_LEVEL";

    public boolean checkInfo(ProfileStage.ProfInfo profile) throws Exception {
        boolean result = false;
        String user = profile.getUser();
        String pass = profile.getPass();

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(new File("").getAbsolutePath()+"/buzzword/saved/Saves.json"));
        JSONObject j = (JSONObject)obj;
        JSONArray profList = (JSONArray)j.get("profile");

        JSONObject jProf = checkLogin(profList,user,pass);
        result = jProf!=null;
        return result;
    }
    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException {
        GameData gameData = (GameData)data;
        String username = gameData.getFirstUser();
        String password = gameData.getFirstPass();
        Set<GameData.ModeProgress> progress = gameData.getProgress();

        JSONParser parser = new JSONParser();
        Object obj = null;
        try { obj = parser.parse(new FileReader(new File("").getAbsolutePath()+"/buzzword/saved/Saves.json")); }
        catch (ParseException e) { e.printStackTrace(); }
        JSONObject j = (JSONObject)obj;
        JSONArray profList = (JSONArray)j.get("profile");

        // Get matching profile by OLD user info, make a new slot if this fails
        JSONObject mainObj = null;
        try { mainObj = checkLogin(profList,username,password); }
        catch (Exception e) { e.printStackTrace(); }

        if(mainObj==null){
            mainObj = new JSONObject();
            profList.add(mainObj);
        }

        // Retrieve progress from the gameData, and apply it to the profile
        JSONArray progArr = new JSONArray();
        for(GameData.ModeProgress mp: progress){
            JSONObject mpo = new JSONObject();
            mpo.put(PROG_NAME,mp.name);
            mpo.put(PROG_LEVEL,mp.level);
            progArr.add(mpo);
        }
        mainObj.put(USERNAME,gameData.getUsername());
        try { mainObj.put(PASSWORD,encrypt(gameData.getPassword(),"Molma100")); }
        catch (Exception e) { e.printStackTrace(); }
        mainObj.put(PROGRESS,progArr);

        //Write the object to the file, overwriting the old profile if it existed.
        FileWriter file = new FileWriter(new File("").getAbsolutePath()+"/buzzword/saved/Saves.json");
        file.write(j.toJSONString());
        file.flush();
        file.close();
    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {
        GameData gameData = (GameData)data;
        String username = gameData.getUsername();
        String password = gameData.getPassword();
        Set<GameData.ModeProgress> progress = new HashSet<>();

        JSONParser parser = new JSONParser();
        Object obj = null;
        try { obj = parser.parse(new FileReader(new File("").getAbsolutePath()+"/buzzword/saved/Saves.json")); }
        catch (ParseException e) { e.printStackTrace();}
        JSONObject j = (JSONObject)obj;
        JSONArray profList = (JSONArray)j.get("profile");

        //Load the proper profile and get the information
        JSONObject mainObj = null;
        try { mainObj = checkLogin(profList,username,password); }
        catch (Exception e) { e.printStackTrace(); }

        if(mainObj != null){
            //Since the user and pass were already verified (and decrypting is failing) we copy them to a new file
            String loadUser = username;
            String loadPass = password;
            JSONArray progArr = (JSONArray)mainObj.get(PROGRESS);
            for(int a=0;a<progArr.size();a++){
                JSONObject loadProg = (JSONObject) progArr.get(a);
                String loadName = (String)loadProg.get(PROG_NAME);
                int loadLevel = toIntExact((long)loadProg.get(PROG_LEVEL));
                GameData.ModeProgress loadedProg = new GameData.ModeProgress(loadName,loadLevel);
                progress.add(loadedProg);
            }
            gameData.setUsername(loadUser);
            gameData.setPassword(loadPass);
            gameData.setProgress(progress);
        }
    }

    private JSONObject checkLogin(JSONArray arr, String username, String password) throws Exception{
        JSONObject result = null;
        for(int a=0;a<arr.size();a++) {
            JSONObject jProf = (JSONObject) arr.get(a);
            String userCheck = (String)jProf.get(USERNAME);
            String passHashCheck = (String)jProf.get(PASSWORD);
            String passHash = encrypt(password,"Molma100");
            if(userCheck.toLowerCase().equals(username.toLowerCase())&&passHashCheck.equals(passHash)){
                result = jProf;
            }
        }
        return result;
    }

    public static String encrypt(String strClearText,String strKey) throws Exception{
        String strData="";

        try {
            SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted=cipher.doFinal(strClearText.getBytes());
            strData=new String(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }

    public static String decrypt(String strEncrypted,String strKey) throws Exception{
        String strData="";

        try {
            SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] decrypted=cipher.doFinal(strEncrypted.getBytes());
            strData=new String(decrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }


    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}
