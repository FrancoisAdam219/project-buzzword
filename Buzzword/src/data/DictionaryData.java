package data;

import apptemplate.AppTemplate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.text.normalizer.Trie;


import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static java.lang.Math.toIntExact;

/**
 * Created by Francois on 12/9/2016.
 */
public class DictionaryData {

	public AppTemplate app;             // Workspace.app

	public class LevelInfo{
		public int letterMin;
		public int targetScore;
	}

	public static final String LETTER_MIN = "letterMin";
	public static final String TARG_SCORE = "targetScore";
	public static final String DICTIONARY_NAME = "name";
	public static final String LEVEL = "levels";

	private String				name;					//name of dictionary
	private File				wordsDir;				//path to words JSON
	private Set<String>			words;					//words in a Set
	private Trie				wordTrie;				//testing...
	private boolean				isLoaded = false;		//check if the data has been loaded
	private Map<Integer,LevelInfo> levels;				//level information for sessions

	public DictionaryData(AppTemplate initApp, String dir){
		app = initApp;
		wordsDir = new File(dir);
	}

	public void loadWords(){
		if(!isLoaded){
			words = new TreeSet<>();
			levels = new HashMap<>();
			JSONObject j = null;
			JSONParser parser = new JSONParser();
			try{
				Object obj = parser.parse(new FileReader(wordsDir.getAbsolutePath()));
				j = (JSONObject)obj;
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			loadLevelInfo(j);
			loadWordList();
			isLoaded = true;
		}
	}

	private void loadLevelInfo(JSONObject j){
		name = (String)j.get(DICTIONARY_NAME);
		JSONArray jaLevel = (JSONArray)j.get(LEVEL);
		for(int a=0;a<jaLevel.size();a++){
			LevelInfo lv = new LevelInfo();
			JSONObject jl = (JSONObject)jaLevel.get(a);
			long letMin = (long)jl.get(LETTER_MIN);
			long targSc = (long)jl.get(TARG_SCORE);
			lv.letterMin = toIntExact(letMin);
			lv.targetScore = toIntExact(targSc);
			levels.put(a+1,lv);
		}
	}
	private void loadWordList(){
		File wordsF = new File(wordsDir.getParent()+"/"+name+".txt");
		//System.out.println(wordsF.getAbsolutePath());
		//long startTime = System.nanoTime();
		try (BufferedReader br = new BufferedReader(new FileReader(wordsF))) {
			String line;
			while((line = br.readLine())!= null){
				words.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//long endTime = System.nanoTime();
		//System.out.println(endTime - startTime);
	}

	public LevelInfo getLevel(int i) {
		LevelInfo result = null;
		if(levels.containsKey(i)){
			//System.out.println("charasdasdf");
			result = levels.get(i);
		}
		//System.out.println(i+": "+result);
		return result;
	}

	public Set<String> getWords() {return words;}
	public int getLevelCount() {return levels.size();}
	public String getName() { return name;}
}
